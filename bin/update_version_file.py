#!/usr/bin/env python3

import os
import json
import argparse
import requests
import boto3
from datetime import datetime

PARSER = argparse.ArgumentParser(description='Update Cloudtokens versions.json file.')
PARSER.add_argument("--force-update",
                    action='store_true',
                    help="Mark release as a forced update.")
ARGS = PARSER.parse_args()

URL = 'http://atl-cloudtoken.s3-website-us-east-1.amazonaws.com/versions.json'
DATA = requests.get(URL, timeout=5.0).json()
VERSION = "{0}.{1}".format(os.environ['BASE_VERSION'], os.environ['BITBUCKET_BUILD_NUMBER'])

if ARGS.force_update:
    if DATA['versions'][-1]['version'] == VERSION:
        DATA['versions'].pop()
DATA['versions'].append({'version': VERSION,
                         'force_update': ARGS.force_update,
                         'build_time': str(datetime.utcnow().isoformat())})

print(json.dumps(DATA, indent=4, sort_keys=True))
S3 = boto3.resource('s3')
S3.Bucket('atl-cloudtoken').put_object(Key='versions.json',
                                       ACL='public-read',
                                       Body=json.dumps(DATA, indent=4, sort_keys=True).encode())
